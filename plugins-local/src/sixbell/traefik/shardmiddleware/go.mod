module sixbell/traefik/shardmiddleware

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gomodule/redigo v1.8.5
	github.com/jkeys089/jserial v1.0.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
