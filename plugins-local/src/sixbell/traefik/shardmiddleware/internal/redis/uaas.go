package redis

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"os"
)

func NewRedisUaasCli() (redis.Conn,error){

	fmt.Println("setting Up new Database Connection")
	redisHost := os.Getenv("REDIS_UAAS_HOSTNAME")
	redisPort := os.Getenv("REDIS_UAAS_PORT")
	//redisPass := os.Getenv("REDIS_UAAS_PASS")
	//redisDB,err := strconv.Atoi(os.Getenv("REDIS_UAAS_DB"))

	c, err := redis.Dial("tcp", redisHost+":"+redisPort)
	if err != nil {
		fmt.Println(err)
		return nil,nil
	}
	//defer c.Close()

	return c,nil
}
