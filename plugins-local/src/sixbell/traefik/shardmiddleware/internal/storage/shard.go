package storage

import (
	"github.com/gomodule/redigo/redis"
)

type ShardRepository struct {
	redisShardCli redis.Conn
}

func NewShardRepository(redisShardCli redis.Conn) *ShardRepository {
	return &ShardRepository{
		redisShardCli: redisShardCli,
	}
}

func (tenant *ShardRepository) GetShardByClientId(clientId int) (string, error) {

	/*
	val, err := tenant.RedisShardCli.Get("clientId").Result()
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	fmt.Println(val)
	return val, nil
	*/

	if clientId == 13 {
		return "1", nil
	}else{
		return "2", nil
	}
}

