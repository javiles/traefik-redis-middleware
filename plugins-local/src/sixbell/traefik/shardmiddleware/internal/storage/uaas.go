package storage

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
)

type UaasRepository struct {
	redisUaasCli redis.Conn
}

func NewUaasRepository(redisUaasCli redis.Conn) *UaasRepository {
	return &UaasRepository{
		redisUaasCli: redisUaasCli,
	}
}

func (uaas *UaasRepository) GetInfoToken(token string) (string,error) {

	//val, err := uaas.RedisUaasCli.Get("auth:"+token).Result()

	val, err := redis.String(uaas.redisUaasCli.Do("GET", "auth:0b7f4f53-c99f-47d9-851d-5865b3adb4fc"))

	if err != nil {
		fmt.Println(err)
		return "",err
	}
	fmt.Println(val)
	return val,nil
}





